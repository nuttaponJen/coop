import Vue from 'vue'
import VueJwtDecode from 'vue-jwt-decode'
import VueMask from 'v-mask'
import Toast from 'vue-toastification'
import 'vue-toastification/dist/index.css'
const numeral = require('numeral')
const moment = require('moment')
const ThaiBaht = require('thai-baht-text')
Vue.use(VueMask)
Vue.use(Toast, {
  transition: 'Vue-Toastification__bounce',
  maxToasts: 20,
  newestOnTop: true
})

Vue.mixin({
  data () {
    return {
      header: {
        headers: {
          'X-Access-Token': sessionStorage.token
        }
      },
      keyexpirecode: 401,
      /* eslint-disable-next-line */
      reg: /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/
    }
  },
  methods: {
    moment,
    checkErrorStatus (text, code) {
      const chk = text.search(code)
      return !(chk < 0)
    },
    checkOAuth () {
      if (!localStorage.token) {
        if (localStorage.refreshToken) {
          // this.checkToken(localStorage.refreshToken)
          this.newAccessToken()
        } else {
          this.ModalBoots('SiginModal').show()
        }
      } else {
        this.callAgentProfile(localStorage.token)
      }
    },
    checkToken (token) {
      const jwt = VueJwtDecode.decode(token)
      const TimoOutLogin = this.daysDiff(jwt.create, jwt.expire)
      if (parseInt(TimoOutLogin) <= 0) {
        this.alert_msg(
          'warning',
          'ตรวจสอบการใช้งาน',
          'ท่านเข้าใช้งานเกินเวลาที่กำหนด'
        )
      }
    },
    alert_msg (type, title, text) {
      const toast = (
        <div>
          <b style="font-size: 20px; padding-top: 7px; margin: 0px; line-height: 18px;">
            {' '}
            {title}{' '}
          </b>
          <br />
          <span>{text}</span>
        </div>
      )

      let icon = ''
      switch (type) {
        case 'info':
          icon = 'fas fa-info-circle fa-2x'
          break
        case 'success':
          icon = 'fas fa-check-circle fa-2x'
          break
        case 'warning':
          icon = 'fas fa-exclamation-circle fa-2x'
          break
        case 'error':
          icon = 'fas fa-times-circle fa-2x'
          break
      }

      this.$toast(toast, {
        type,
        position: 'top-right',
        timeout: 2000,
        closeOnClick: true,
        pauseOnFocusLoss: true,
        pauseOnHover: true,
        draggable: true,
        draggablePercent: 0.6,
        showCloseButtonOnHover: false,
        hideProgressBar: true,
        closeButton: 'button',
        icon,
        rtl: false
      })
    },
    reload () {
      location.reload()
    },
    isEmailValid (mail) {
      return mail === '' ? '' : !this.reg.test(mail)
    },
    ModalBoots (Id) {
      /* eslint-disable-next-line */
      return new bootstrap.Modal(document.getElementById(Id))
    },
    daysDiff (d1, d2) {
      const startDate = moment(d1).format('x')
      // Do your operations
      const endDate = moment(d2).format('x')
      const seconds = (endDate - startDate) / 60000
      return Math.floor(seconds)
    },
    getLastDate (Data) {
      const res = Data.split('-')
      const month = res[1] // January
      const year = res[0]
      const d = new Date(year, month, 0)
      return moment(d).format('DD')
    },
    dateYearThai (date) {
      return parseInt(moment(date).format('YYYY')) + 543
    },
    dateMonth (date) {
      return moment(date).format('YYYY-MM')
    },
    dateFormatAndTime (date) {
      moment.locale()
      return moment(date).format('DD/MM/YYYY HH:mm')
    },
    dateFormatFullTime (date) {
      moment.locale()
      return moment(date).format('YYYY-MM-DD HH:mm:ss')
    },
    dateFullTime (date) {
      moment.locale()
      return moment(date).format('HH:mm:ss')
    },
    dateFormatDMY (date) {
      moment.locale()
      return moment(date).format('DD/MM/YYYY')
    },
    dateFormatYMD (date) {
      moment.locale()
      return moment(date).format('YYYY-MM-DD')
    },
    dateFormatMY (date) {
      moment.locale()
      return moment(date).format('MM/YYYY')
    },
    dateFormatMil (date) {
      moment.locale()
      return moment(date).format()
    },
    // monthFormat (str) {
    //   switch (str) {
    //     case '01':
    //       return 'ม.ค'
    //     case '02':
    //       return 'ก.พ'
    //     case '03':
    //       return 'มี.ค'
    //     case '04':
    //       return 'เม.ย'
    //     case '05':
    //       return 'พ.ค'
    //     case '06':
    //       return 'มิ.ย'
    //     case '07':
    //       return 'ก.ค'
    //     case '08':
    //       return 'ส.ค'
    //     case '09':
    //       return 'ก.ย'
    //     case '10':
    //       return 'ต.ค'
    //     case '11':
    //       return 'พ.ย'
    //     default:
    //       return 'ธ.ค'
    //   }
    // },
    monthYearFormat (str) {
      const date = str.split('-')
      switch (date[1]) {
        case '01':
          return 'มกราคม ' + date[0]
        case '02':
          return 'กุมภาพันธ์ ' + date[0]
        case '03':
          return 'มีนาคม ' + date[0]
        case '04':
          return 'เมษายน ' + date[0]
        case '05':
          return 'พฤษภาคม ' + date[0]
        case '06':
          return 'มิถุนายน ' + date[0]
        case '07':
          return 'กรกฎาคม ' + date[0]
        case '08':
          return 'สิงหาคม ' + date[0]
        case '09':
          return 'กันยายน ' + date[0]
        case '10':
          return 'ตุลาคม ' + date[0]
        case '11':
          return 'พฤศจิกายน ' + date[0]
        default:
          return 'ธันวาคม ' + date[0]
      }
    },
    numberFormatDecimal (str) {
      return numeral(str).format('0,0.00')
    },
    numberFormat (str) {
      return numeral(str).format('0,0')
    },
    moneyText (str) {
      return ThaiBaht(str)
    },
    convert_vs (val) {
      const data = []
      let len = 3
      if (val.length > 3) {
        len = val.length
      }
      let vallen = val.length
      for (let i = 0; i < len; i++) {
        const v = val.split('')
        if (len - i === vallen) {
          const ss = len - i
          if (ss === vallen) {
            if (val.length === 1) {
              data.push(val)
            } else if (len > val.length) {
              data.push(v[i - 1])
            } else {
              data.push(v[i])
            }
          } else {
            data.push(v[i - 1])
          }
          vallen--
        } else {
          data.push('0')
        }
      }
      let versions = ''
      for (let l = 0; l < data.length; l++) {
        versions += data[l]
        if (l < data.length - 1) {
          versions += '.'
        }
      }
      return versions
    },
    getParam (param) {
      const url = new URL(window.location.href)
      const c = url.searchParams.get(param)
      return c
    },
    DataURIToBlob (dataURI) {
      const binary = atob(dataURI.split(',')[1])
      const array = []
      for (let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i))
      }
      return new Blob([new Uint8Array(array)], { type: 'image/jpeg' })
    },
    playSound (sound) {
      if (sound) {
        const audio = new Audio(sound)
        audio.play()
      }
    },
    async globalShare (name, url) {
      const shareData = {
        title: 'PAY9 Agent',
        text: 'Pay9 ' + name,
        url
      }
      try {
        await navigator.share(shareData)
      } catch (err) {
        console.log(err)
      }
    }
  }
})

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    approve:0,
    maintenance:0,
    accesscode: "",
    header: {
      headers: {
        'Access-Token': sessionStorage.token
      }
    },
  },
 
  mutations: {

  },
  actions: {

  },
  modules: {

  }
})

import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)
const address = 'https://asset-api.nopadol.net'
// const address = 'http://10.0.3.226:3001'
export default {
  LoginAttrs(obj, success, error) { // Login token
    Vue.axios.post(address + '/auth/signin', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  // ------------------ Dashboard
  AdminDashboard(header,obj, success, error) {
    Vue.axios.post(address + '/asset_api/dashboard/count/assetitem', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  // ------------------ Manage User
  ListUser(header,obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListDepartment(obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/position', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertUser(header ,obj, success, error) {
    Vue.axios.post(address + '/asset_api/create/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UpdateUser(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  DeleteUser(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/dashboard/count/assetitem', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectUser(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/get/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ChangeUserStatus(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/user/active', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertCategory(header ,obj, success, error) { 
    Vue.axios.post(address + '/asset_api/create/category', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/category', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/get/category/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UpdateCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/category', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  DeleteCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/delete/category/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListSubCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/subcategory', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },

  InsertSubCategory(header,obj, success, error) {
    Vue.axios.post(address + '/asset_api/create/subcategory', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UpdateSubCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/subcategory', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectSubCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/get/subcategory/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  DeleteSubCategory(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/delete/subcategory/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  // --------------------------- Asset
  SerachAssetCatSub(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/categorybar', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SerachAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/search/asset/item', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SerachAssetBycat(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/search/asset/category', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SerachAsset(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/search/asset', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListAsset(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/asset', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertAsset(header,obj, success, error) {
    Vue.axios.post(address + '/asset_api/create/asset', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },

  genItem(header, obj, success, error) {
    Vue.axios.post(address + '/upload/asset', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  HaederAsset(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/get/asset/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UpdateAsset(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/asset', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  DeleteAsset(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/delete/asset/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },

  // --------------------------- AssetItem
  ListAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/asset/item', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  LocationAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/location', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectLocationAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/get/location/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/create/asset/item', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListAssetItemAvailable(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/asset/item/available', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListAssetItemUnavailable(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/asset/item/unavailable', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListAssetItemDetail(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/get/asset/item/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UpdateAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/item', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  DeleteAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/delete/asset/item/id', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UpdatestatusAssetItem(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/status', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  CaldateWarrant(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/calwarrantydate', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  Caldateprice(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/calprices', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  //____________ Asset Damage________________
  ListAssetDamage(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/asset/damage', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  //____________ AssetItem Damage________________
  ListAssetItemDamage(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/asset/item/damage', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ChangStatusDamage(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/update/asset/damage', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  //_________________ Approve_______________________
  ListApprovewithdrawn(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/list/request', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertApprovedwithdrawn(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/create/request', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ChangeApprovewithdrawnReq(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/accept/request', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SendApprovewithdrawnReject(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/decline/request', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectApprovewithdrawn(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/get/request', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectApproveDetail(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/get/transaction', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  AcceptDepositAsset(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/accept/return', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  //________________________ Maintainence ______________________
  MaintenanceReq(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/create/repair', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  QrcodeMaintenance( obj, success, error) {
    Vue.axios.post(address + '/transaction_api/qrcode', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectMaintenance( obj, success, error) {
    Vue.axios.post(address + '/transaction_api/get/repair', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  MaintenanceConfirm(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/manage/repair', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  SelectItemDetail( obj, success, error) {
  Vue.axios.post(address + '/asset_api/get/asset/item/id/bp' , obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertScanWithdraw(obj, success, error) {
    Vue.axios.post(address + '/transaction_api/create/request/bp', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertScanMaintenance( obj, success, error) {
    Vue.axios.post(address + '/transaction_api/create/repair/bp', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  InsertScanDeposit(obj, success, error) {
    Vue.axios.post(address + '/transaction_api/create/return/bp', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ChangUserItem(obj, success, error) {
    Vue.axios.post(address + '/transaction_api/create/change/bp', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListUsername(obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/name', obj).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  // ______________________ Notify ______________________
  Notify(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/get/qtytransaction', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  // ______________________ User ______________________
  UserApprove(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/list/request', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListAssetUser(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/list/asset/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UserSerachAssetBycat(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/search/asset/category/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  UserSerachAsset(header, obj, success, error) {
    Vue.axios.post(address + '/asset_api/search/asset/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  ListInventory(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/list/request/user', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  DepositAsset(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/create/return', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  CancelUserWithdrawRequest(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/cancel/request', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  CancelUserDepositRequest(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/cancel/return', obj, header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
  CancelUserMantainRequest(header, obj, success, error) {
    Vue.axios.post(address + '/transaction_api/cancel/report', obj ,header).then(
      (response) => {
        success(response.data)
      },
      (response) => {
        error(response)
      }
    )
  },
}




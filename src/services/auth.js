export default {
      getToken() {
            return sessionStorage.token;
      },

      logout(cb) {
            delete sessionStorage.user_code;
            delete sessionStorage.name;
            delete sessionStorage.token;
            delete sessionStorage.userAccess;
            delete sessionStorage.pageActive; 
            if (cb) cb();
            this.onChange(false);
      },

      loggedIn() {
            return !!sessionStorage.token;
      },
      onChange() { }
};
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Full from "@/components/container/full";
// User=====================================
import ManageAsset from '@/views/admin/Asset/ManageAsset'
import ManageCategory from '@/views/admin/Category/ManageCategory'
import ListAssetitem from '@/views/admin/Asset/listAssetitem'
import ManageAssetDamage from '@/views/admin/AssetDamage/ManageAssetDamage'
import ListAssetitemDamage from '@/views/admin/AssetDamage/listAssetitemDamage'
import Dashboard from '@/views/admin/Dashboard'
import manageApproveAsset from '@/views/admin/Approve/manageApproveAsset'
import ApproveRequest from '@/views/admin/Approve/ApproveRequest'
import manageMaintenanceAsset from '@/views/admin/Maintenance/manageMaintenanceAsset'
import PrintQrcodeMaintenance from '@/views/admin/Approve/PrintQrcodeMaintenance'
import manageAccount from '@/views/admin/Account/manageAccount'
import Report from '@/views/admin/Report/Report'
import InsertReport from '@/views/admin/Report/InsertReport'

// User=====================================
import Asset from '@/views/user/Asset'
import AssetHistory from '@/views/user/AssetHistory'
import Login from '@/components/Login'
import MaintenanceAsset from '@/views/user/MaintenanceAsset'
import Inventory from '@/views/user/Inventory'
import Account from '@/views/user/Account'
import Tutorial from '../views/Tutorial'
// Outer

import RepairRequest from '@/views/user/RepairRequest'
Vue.use(VueRouter)

const routes = [
  //=========================Admin=========================//
  {
    path: "/",
    redirect: "/",
    name: "main",
    component: Full,
    children: [
      {
        path: '/ManageAsset',
        name: 'ManageAsset',
        component: ManageAsset
      },
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: '/ManageCategory',
        name: 'ManageCategory',
        component: ManageCategory
      },
      {
        path: '/ListAssetitem/:id',
        name: 'ListAssetitem',
        component: ListAssetitem
      },
      {
        path: '/ManageAssetDamage',
        name: 'ManageAssetDamage',
        component: ManageAssetDamage
      },
      {
        path: '/ListAssetitemDamage/:id',
        name: 'ListAssetitemDamage',
        component: ListAssetitemDamage
      },
      {
        path: '/Dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: '/manageApproveAsset',
        name: 'manageApproveAsset',
        component: manageApproveAsset
      },
      {
        path: '/manageAccount',
        name: 'manageAccount',
        component: manageAccount
      },
      {
        path: '/Report',
        name: 'Report',
        component: Report
      },

      {
        path: '/InsertReport',
        name: 'InsertReport',
        component: InsertReport
      },
      //=========================User=========================//
      {
        path: '/Asset',
        name: 'Asset',
        component: Asset
      },
      {
        path: '/AssetHistory',
        name: 'AssetHistory',
        component: AssetHistory
      },
      {
        path: '/MaintenanceAsset',
        name: 'MaintenanceAsset',
        component: MaintenanceAsset
      },
      {
        path: '/Inventory',
        name: 'Inventory',
        component: Inventory
      },
      {
        path: '/Account/:id',
        name: 'Account',
        component: Account
      },
      //======================== Link Scan ==========================//
      {
        path: '/ApproveRequest/:id',
        name: 'ApproveRequest',
        component: ApproveRequest
      },
      {
        path: '/manageMaintenanceAsset',
        name: 'manageMaintenanceAsset',
        component: manageMaintenanceAsset
      },
      {
        path: '/Tutorial',
        name: 'Tutorial',
        component: Tutorial
      },
    ]
  },
  {
    path: '/RepairRequest/:id',
    name: 'RepairRequest',
    component: RepairRequest
  },
  {
    path: '/PrintQrcodeMaintenance/:id',
    name: 'PrintQrcodeMaintenance',
    component: PrintQrcodeMaintenance
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
